/*
This test case is automatically extracted from
swtpm source code and manually modified to integrate in Libtasn1 test suite.
This test case will build an ASN1 structure and encode it.

SWTPM license

-------

3-clause BSD license

(c) Copyright IBM Corporation 2006, 2010.

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

Neither the names of the IBM Corporation nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
#include <libtasn1.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct
{
  unsigned char *data;
  unsigned int size;
} gnutls_datum_t;

const asn1_static_node tpm_asn1_tab[] = { {"TPM", 536875024, NULL},
{NULL, 1073741836, NULL},
{"tcg", 1879048204, NULL},
{"joint-iso-itu-t", 1073741825, "2"},
{"international-organizations", 1073741825, "23"},
{"tcg", 1, "133"},
{"tcg-attribute", 1879048204, NULL},
{NULL, 1073741825, "tcg"},
{NULL, 1, "2"},
{"tcg-at-tpmManufacturer", 1879048204, NULL},
{NULL, 1073741825, "tcg-attribute"},
{NULL, 1, "1"},
{"tcg-at-tpmModel", 1879048204, NULL},
{NULL, 1073741825, "tcg-attribute"},
{NULL, 1, "2"},
{"tcg-at-tpmVersion", 1879048204, NULL},
{NULL, 1073741825, "tcg-attribute"},
{NULL, 1, "3"},
{"tcg-at-platformManufacturer", 1879048204, NULL},
{NULL, 1073741825, "tcg-attribute"},
{NULL, 1, "4"},
{"tcg-at-platformModel", 1879048204, NULL},
{NULL, 1073741825, "tcg-attribute"},
{NULL, 1, "5"},
{"tcg-at-platformVersion", 1879048204, NULL},
{NULL, 1073741825, "tcg-attribute"},
{NULL, 1, "6"},
{"tcg-at-tpmSpecification", 1879048204, NULL},
{NULL, 1073741825, "tcg-attribute"},
{NULL, 1, "16"},
{"tcg-kp-EKCertificate", 1879048204, NULL},
{"joint-iso-itu-t", 1073741825, "2"},
{"international-organizations", 1073741825, "23"},
{"tcg", 1073741825, "133"},
{"kp", 1073741825, "8"},
{NULL, 1, "1"},
{"TPMSpecificationInfo", 1610612741, NULL},
{"tpmSpecificationSeq", 2, "TPMSpecificationSeq"},
{"TPMSpecificationSeq", 1610612741, NULL},
{"id", 1073741836, NULL},
{"tpmSpecificationSet", 2, "TPMSpecificationSet"},
{"TPMSpecificationSet", 1610612750, NULL},
{"tpmSpecification", 201326594, "TPMSpecification"},
{"TPMSpecification", 1610612741, NULL},
{"family", 1073741858, NULL},
{"level", 1073741827, NULL},
{"revision", 3, NULL},
{"TPMManufacturerInfo", 1610612741, NULL},
{"tpmManufacturerSet", 1073741826, "TPMManufacturerSet"},
{"tpmModelSet", 1073741826, "TPMModelSet"},
{"tpmVersionSet", 2, "TPMVersionSet"},
{"TPMManufacturerSet", 1610612750, NULL},
{"tpmManufacturer", 201326594, "TPMManufacturer"},
{"TPMManufacturer", 1610612741, NULL},
{"id", 1073741836, NULL},
{"manufacturer", 34, NULL},
{"TPMModelSet", 1610612750, NULL},
{"tpmModel", 201326594, "TPMModel"},
{"TPMModel", 1610612741, NULL},
{"id", 1073741836, NULL},
{"model", 34, NULL},
{"TPMVersionSet", 1610612750, NULL},
{"tpmVersion", 201326594, "TPMVersion"},
{"TPMVersion", 1610612741, NULL},
{"id", 1073741836, NULL},
{"version", 34, NULL},
{"PlatformManufacturerInfo", 1610612741, NULL},
{"platformManufacturerSet", 1073741826, "PlatformManufacturerSet"},
{"platformModelSet", 1073741826, "PlatformModelSet"},
{"platformVersionSet", 2, "PlatformVersionSet"},
{"PlatformManufacturerSet", 1610612750, NULL},
{"platformManufacturer", 201326594, "PlatformManufacturer"},
{"PlatformManufacturer", 1610612741, NULL},
{"id", 1073741836, NULL},
{"manufacturer", 34, NULL},
{"PlatformModelSet", 1610612750, NULL},
{"platformModel", 201326594, "PlatformModel"},
{"PlatformModel", 1610612741, NULL},
{"id", 1073741836, NULL},
{"model", 34, NULL},
{"PlatformVersionSet", 1610612750, NULL},
{"platformVersion", 201326594, "PlatformVersion"},
{"PlatformVersion", 1610612741, NULL},
{"id", 1073741836, NULL},
{"version", 34, NULL},
{"PlatformCertificateSAN", 1610612747, NULL},
{NULL, 13, NULL},
{"TPMEKCertExtendedKeyUsage", 536870917, NULL},
{"id", 12, NULL},
{NULL, 0, NULL}
};

asn1_node _tpm_asn;

static int asn_init (void);
static int encode_asn1 (gnutls_datum_t * asn1, asn1_node at);
static int
build_tpm_manufacturer_info (asn1_node * at, const char *manufacturer,
			     const char *tpm_model, const char *tpm_version);
static int
build_platf_manufacturer_info (asn1_node * at, const char *manufacturer,
			       const char *platf_model,
			       const char *platf_version, bool forTPM2);
int Test_create_tpm_and_platform_manuf_info (void);
int main (void);

static int
asn_init (void)
{
  static bool inited;
  int err;

  if (inited)
    return ASN1_SUCCESS;

  err = asn1_array2tree (tpm_asn1_tab, &_tpm_asn, NULL);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "array2tree error: %d", err);
      goto cleanup;
    }

  inited = true;

cleanup:

  return err;
}

static int
encode_asn1 (gnutls_datum_t * asn1, asn1_node at)
{
  int err;

  /* determine needed size of byte array */
  asn1->size = 0;
  err = asn1_der_coding (at, "", NULL, (int *) &asn1->size, NULL);
  if (err != ASN1_MEM_ERROR)
    {
      fprintf (stderr, "1. asn1_der_coding error: %d\n", err);
      return err;
    }

  asn1->data = (unsigned char *) malloc (asn1->size + 16);
  if (!asn1->data)
    {
      fprintf (stderr, "2. Could not allocate memory\n");
      return ASN1_MEM_ERROR;
    }

  err = asn1_der_coding (at, "", asn1->data, (int *) &asn1->size, NULL);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "3. asn1_der_coding error: %d\n", err);
      free (asn1->data);
      asn1->data = NULL;
    }
  return err;
}

static int
build_tpm_manufacturer_info (asn1_node * at, const char *manufacturer,
			     const char *tpm_model, const char *tpm_version)
{
  int err;

  err = asn1_create_element (_tpm_asn, "TPM.TPMManufacturerInfo", at);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "asn1_create_element error: %d\n", err);
      goto cleanup;
    }

  err = asn1_write_value (*at, "tpmManufacturerSet.tpmManufacturer.?LAST",
			  "NEW", 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "1a. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err = asn1_write_value (*at, "tpmManufacturerSet.tpmManufacturer.id",
			  "2.23.133.2.1", 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "1b. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err =
    asn1_write_value (*at,
		      "tpmManufacturerSet.tpmManufacturer.manufacturer",
		      manufacturer, 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "2. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err = asn1_write_value (*at, "tpmModelSet.tpmModel.?LAST", "NEW", 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "3a. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err = asn1_write_value (*at, "tpmModelSet.tpmModel.id", "2.23.133.2.2", 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "3b. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err = asn1_write_value (*at, "tpmModelSet.tpmModel.model", tpm_model, 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "4. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err = asn1_write_value (*at, "tpmVersionSet.tpmVersion.?LAST", "NEW", 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "5a. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err =
    asn1_write_value (*at, "tpmVersionSet.tpmVersion.id", "2.23.133.2.3", 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "5b. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err =
    asn1_write_value (*at, "tpmVersionSet.tpmVersion.version",
		      tpm_version, 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "6. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

cleanup:
  return err;
}

static int
build_platf_manufacturer_info (asn1_node * at, const char *manufacturer,
			       const char *platf_model,
			       const char *platf_version, bool forTPM2)
{
  int err;

  err = asn1_create_element (_tpm_asn, "TPM.PlatformManufacturerInfo", at);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "asn1_create_element error: %d\n", err);
      goto cleanup;
    }

  err =
    asn1_write_value (*at,
		      "platformManufacturerSet.platformManufacturer.?LAST",
		      "NEW", 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "b1a. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err = asn1_write_value (*at,
			  "platformManufacturerSet.platformManufacturer.id",
			  forTPM2 ? "2.23.133.5.1.1" : "2.23.133.2.4", 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "b1b. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err =
    asn1_write_value (*at,
		      "platformManufacturerSet.platformManufacturer.manufacturer",
		      manufacturer, 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "b2. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err =
    asn1_write_value (*at, "platformModelSet.platformModel.?LAST", "NEW", 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "b3a. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err = asn1_write_value (*at, "platformModelSet.platformModel.id",
			  forTPM2 ? "2.23.133.5.1.4" : "2.23.133.2.5", 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "b3b. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err = asn1_write_value (*at, "platformModelSet.platformModel.model",
			  platf_model, 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "b4. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err = asn1_write_value (*at, "platformVersionSet.platformVersion.?LAST",
			  "NEW", 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "b5a. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err = asn1_write_value (*at, "platformVersionSet.platformVersion.id",
			  forTPM2 ? "2.23.133.5.1.5" : "2.23.133.2.6", 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "b5b. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

  err =
    asn1_write_value (*at, "platformVersionSet.platformVersion.version",
		      platf_version, 0);
  if (err != ASN1_SUCCESS)
    {
      fprintf (stderr, "b6. asn1_write_value error: %d\n", err);
      goto cleanup;
    }

cleanup:
  return err;
}

int
Test_create_tpm_and_platform_manuf_info (void)
{
  /* begin function parameters */
  const char *tpm_manufacturer = "IBM";
  const char *tpm_model = "swtpm-libtpms";
  const char *tpm_version = "1.2";
  const char *platf_manufacturer = "Fedora";
  const char *platf_model = "QEMU";
  const char *platf_version = "2.1";
  bool forTPM2 = false;
  /* end function parameters */
  asn1_node at = NULL;
  asn1_node tpm_at = NULL;
  asn1_node platf_at = NULL;
  int err;
  gnutls_datum_t datum = {
    .data = NULL,
    .size = 0,
  };
  err = asn_init ();
  if (err != ASN1_SUCCESS)
    goto cleanup;
  err = asn1_create_element (_tpm_asn, "TPM.PlatformCertificateSAN", &at);
  if (err != ASN1_SUCCESS)
    goto cleanup;
  err = build_tpm_manufacturer_info (&tpm_at, tpm_manufacturer, tpm_model,
				     tpm_version);
  if (err != ASN1_SUCCESS)
    goto cleanup;
  err = encode_asn1 (&datum, tpm_at);
  if (err != ASN1_SUCCESS)
    goto cleanup;
  err = asn1_write_value (at, "", "NEW", 1);
  if (err != ASN1_SUCCESS)
    goto cleanup;
  err = asn1_write_value (at, "?1", datum.data, datum.size);
  if (err != ASN1_SUCCESS)
    goto cleanup;
  free (datum.data);
  datum.data = NULL;
  err = build_platf_manufacturer_info (&platf_at, platf_manufacturer,
				       platf_model, platf_version, forTPM2);
  if (err != ASN1_SUCCESS)
    goto cleanup;
  err = encode_asn1 (&datum, platf_at);
  if (err != ASN1_SUCCESS)
    goto cleanup;
  err = asn1_write_value (at, "", "NEW", 1);
  if (err != ASN1_SUCCESS)
    goto cleanup;
cleanup:
  free (datum.data);
  asn1_delete_structure (&at);
  asn1_delete_structure (&platf_at);
  asn1_delete_structure (&tpm_at);
  return err;
}

int
main (void)
{
  if (Test_create_tpm_and_platform_manuf_info () != ASN1_SUCCESS)
    return -1;
  return 0;
}
