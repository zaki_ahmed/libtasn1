/*
This test is automatically extracted from
GnuTLS source code and manually modified to integrate in Libtasn1 test suite.
This test will create an element then fill it with the values of a DER encoding
string. It then attempts to read a value from that element.

 * The GnuTLS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

*/
#include <libtasn1.h>
#include <stdint.h>

static asn1_node _gnutls_pkix1_asn = NULL;
static asn1_node _gnutls_gnutls_asn = NULL;

const asn1_static_node gnutls_asn1_tab[] = { {"GNUTLS", 536872976, NULL},
{NULL, 1073741836, NULL},
{"RSAPublicKey", 1610612741, NULL},
{"modulus", 1073741827, NULL},
{"publicExponent", 3, NULL},
{"RSAPrivateKey", 1610612741, NULL},
{"version", 1073741827, NULL},
{"modulus", 1073741827, NULL},
{"publicExponent", 1073741827, NULL},
{"privateExponent", 1073741827, NULL},
{"prime1", 1073741827, NULL},
{"prime2", 1073741827, NULL},
{"exponent1", 1073741827, NULL},
{"exponent2", 1073741827, NULL},
{"coefficient", 1073741827, NULL},
{"otherPrimeInfos", 16386, "OtherPrimeInfos"},
{"ProvableSeed", 1610612741, NULL},
{"algorithm", 1073741836, NULL},
{"seed", 7, NULL},
{"OtherPrimeInfos", 1612709899, NULL},
{"MAX", 1074266122, "1"},
{NULL, 2, "OtherPrimeInfo"},
{"OtherPrimeInfo", 1610612741, NULL},
{"prime", 1073741827, NULL},
{"exponent", 1073741827, NULL},
{"coefficient", 3, NULL},
{"AlgorithmIdentifier", 1610612741, NULL},
{"algorithm", 1073741836, NULL},
{"parameters", 541081613, NULL},
{"algorithm", 1, NULL},
{"DigestInfo", 1610612741, NULL},
{"digestAlgorithm", 1073741826, "DigestAlgorithmIdentifier"},
{"digest", 7, NULL},
{"DigestAlgorithmIdentifier", 1073741826, "AlgorithmIdentifier"},
{"DSAPublicKey", 1073741827, NULL},
{"DSAParameters", 1610612741, NULL},
{"p", 1073741827, NULL},
{"q", 1073741827, NULL},
{"g", 3, NULL},
{"DSASignatureValue", 1610612741, NULL},
{"r", 1073741827, NULL},
{"s", 3, NULL},
{"DSAPrivateKey", 1610612741, NULL},
{"version", 1073741827, NULL},
{"p", 1073741827, NULL},
{"q", 1073741827, NULL},
{"g", 1073741827, NULL},
{"Y", 1073741827, NULL},
{"priv", 3, NULL},
{"DHParameter", 1610612741, NULL},
{"prime", 1073741827, NULL},
{"base", 1073741827, NULL},
{"privateValueLength", 16387, NULL},
{"pkcs-11-ec-Parameters", 1610612754, NULL},
{"oId", 1073741836, NULL},
{"curveName", 31, NULL},
{"ECParameters", 1610612754, NULL},
{"namedCurve", 12, NULL},
{"ECPrivateKey", 1610612741, NULL},
{"Version", 1073741827, NULL},
{"privateKey", 1073741831, NULL},
{"parameters", 1610637314, "ECParameters"},
{NULL, 2056, "0"},
{"publicKey", 536895494, NULL},
{NULL, 2056, "1"},
{"PrincipalName", 1610612741, NULL},
{"name-type", 1610620931, NULL},
{NULL, 2056, "0"},
{"name-string", 536879115, NULL},
{NULL, 1073743880, "1"},
{NULL, 27, NULL},
{"KRB5PrincipalName", 1610612741, NULL},
{"realm", 1610620955, NULL},
{NULL, 2056, "0"},
{"principalName", 536879106, "PrincipalName"},
{NULL, 2056, "1"},
{"RSAPSSParameters", 1610612741, NULL},
{"hashAlgorithm", 1610637314, "AlgorithmIdentifier"},
{NULL, 2056, "0"},
{"maskGenAlgorithm", 1610637314, "AlgorithmIdentifier"},
{NULL, 2056, "1"},
{"saltLength", 1610653699, NULL},
{NULL, 1073741833, "20"},
{NULL, 2056, "2"},
{"trailerField", 536911875, NULL},
{NULL, 1073741833, "1"},
{NULL, 2056, "3"},
{"GOSTParameters", 1610612741, NULL},
{"publicKeyParamSet", 1073741836, NULL},
{"digestParamSet", 16396, NULL},
{"GOSTParametersOld", 1610612741, NULL},
{"publicKeyParamSet", 1073741836, NULL},
{"digestParamSet", 1073741836, NULL},
{"encryptionParamSet", 16396, NULL},
{"GOSTPrivateKey", 1073741831, NULL},
{"GOSTPrivateKeyOld", 1073741827, NULL},
{"IssuerSignTool", 1610612741, NULL},
{"signTool", 1073741858, NULL},
{"cATool", 1073741858, NULL},
{"signToolCert", 1073741858, NULL},
{"cAToolCert", 34, NULL},
{"Gost28147-89-EncryptedKey", 1610612741, NULL},
{"encryptedKey", 1073741831, NULL},
{"maskKey", 1610637319, NULL},
{NULL, 4104, "0"},
{"macKey", 7, NULL},
{"SubjectPublicKeyInfo", 1610612741, NULL},
{"algorithm", 1073741826, "AlgorithmIdentifier"},
{"subjectPublicKey", 6, NULL},
{"GostR3410-TransportParameters", 1610612741, NULL},
{"encryptionParamSet", 1073741836, NULL},
{"ephemeralPublicKey", 1610637314, "SubjectPublicKeyInfo"},
{NULL, 4104, "0"},
{"ukm", 7, NULL},
{"GostR3410-KeyTransport", 1610612741, NULL},
{"sessionEncryptedKey", 1073741826, "Gost28147-89-EncryptedKey"},
{"transportParameters", 536895490, "GostR3410-TransportParameters"},
{NULL, 4104, "0"},
{"TPMKey", 536870917, NULL},
{"type", 1073741836, NULL},
{"emptyAuth", 1610637316, NULL},
{NULL, 2056, "0"},
{"parent", 1073741827, NULL},
{"pubkey", 1073741831, NULL},
{"privkey", 7, NULL},
{NULL, 0, NULL}
};

static int _gnutls_global_init (void);
int Test__gnutls_x509_read_ecc_params (void);
int main (void);

static int
_gnutls_global_init (void)
{
  _gnutls_pkix1_asn = NULL;

  return asn1_array2tree (gnutls_asn1_tab, &_gnutls_gnutls_asn, NULL);
}

int
Test__gnutls_x509_read_ecc_params (void)
{
  /* begin function parameters */

  uint8_t der[] = { 0x6, 0x8, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x3, 0x1, 0x7 };
  int dersize = 10;
  /* end function parameters */
  int ret;
  asn1_node spk = NULL;
  char oid[128];
  int oid_size;
  ret = _gnutls_global_init ();
  if (ret != ASN1_SUCCESS)
    return ret;
  if ((ret = asn1_create_element (((asn1_node) _gnutls_gnutls_asn),
				  "GNUTLS.ECParameters", &spk))
      != ASN1_SUCCESS)
    return ret;
  ret = asn1_der_decoding (&spk, der, dersize, NULL);
  if (ret != ASN1_SUCCESS)
    return ret;
  oid_size = sizeof (oid);
  ret = asn1_read_value (spk, "namedCurve", oid, &oid_size);
  if (ret != ASN1_SUCCESS)
    return ret;
  asn1_delete_structure (&spk);
  return ret;
}

int
main (void)
{
  if (Test__gnutls_x509_read_ecc_params () != ASN1_SUCCESS)
    return -1;
  return 0;
}
